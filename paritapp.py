from __future__ import unicode_literals
import time
import dateparser
from glob import glob
import pandas as pd
import matplotlib.pyplot as plt

grilla = pd.read_excel('tidy_escala_salarial.xlsx')
ipc = pd.read_csv('IPC San Luis LNViz - Hoja 1.csv', skiprows=[1], decimal=',',
                  encoding='utf8')
ipc['Fecha'] = pd.to_datetime(ipc['Fecha'], dayfirst=True)
ipc = ipc[ipc['Fecha'] >= grilla['Fecha'].min()]
ipc[u'Variación Mensual Mult'] = 1 + ipc[u'Variación Mensual']/100
ipc['Variacón Acumulada'] = ipc[u'Variación Mensual Mult'].cumprod()
cargo = 'AYUDANTE DE PRIMERA'
dedicacion = 'PARC/SIMPLE'
anti = 0
grilla_filt = grilla[(grilla['Cargo'] == cargo) & (grilla['Dedicacion'] == dedicacion) & (grilla[u'Antiguedad Años'] == anti)]
df = ipc.merge(grilla_filt, how='left', on='Fecha')
df = df.fillna(method='ffill')
df['Sueldo Bruto Corregido'] = df['Sueldo Bruto']/df['Variacón Acumulada']
df.plot(x='Fecha', y='Sueldo Bruto')
df.plot(x='Fecha', y='Sueldo Bruto Corregido')
plt.show()
